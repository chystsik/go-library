package main

import (
	"log"

	"gitlab.com/chystsik/go-library/app"
	"gitlab.com/chystsik/go-library/config"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	app.Run(cfg)
}
