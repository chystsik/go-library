package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/chystsik/go-library/config"
	"gitlab.com/chystsik/go-library/internal/adapters"
	"gitlab.com/chystsik/go-library/internal/infrastructure/parser"
	"gitlab.com/chystsik/go-library/internal/infrastructure/repository"
	"gitlab.com/chystsik/go-library/internal/service"
	"gitlab.com/chystsik/go-library/internal/transport/restapi"

	"github.com/sirupsen/logrus"
)

const (
	errHttpListen   = "Failed to listen and serve (http): %v\n"
	errHttpShutdown = "HTTP server shutdown error: %v\n"
	errDbDisconnect = "%v DB client disconnect error: %v\n"

	logHttpStart       = "HTTP Server: started on %s:%d\n"
	logHttpStop        = "HTTP Server: stopped serving new connections..."
	logHttpShutdown    = "Graceful shutdown of HTTP Server complete."
	logSignalInterrupt = "Interrupt signal. Shutdown"
	logDbConnect       = "Successfully connected to %s and pinged."
	logDbDisconnect    = "Graceful close connection for %v client complete.\n"
)

func Run(cfg *config.AppConfig) {
	// Initialising logger
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: time.RFC3339,
	})
	level := logrus.Level(cfg.Service.Loglevel)
	logger.SetLevel(level)

	// Initialising repository, service and handlers
	vacancyRepository, err := repository.NewVacancyRepository(cfg.Database, logger)
	if err != nil {
		logger.Fatal(err)
	}

	err = vacancyRepository.Connect(context.TODO())
	if err != nil {
		logger.Fatal(err)
	}
	logger.Infof(logDbConnect, cfg.Database.Provider)

	vacancyParser := parser.NewVacancyParser(cfg.Parser, logger)
	vacancyService := service.NewVacancyService(vacancyRepository, vacancyParser)
	handlers := adapters.NewHandlers(vacancyService, cfg.Service.SwaggerUiFilePath, logger)

	// Startup http server with handlers
	server := restapi.NewServer(*cfg, handlers, logger)
	go func() {
		logger.Infof(logHttpStart, cfg.Http.Host, cfg.Http.Port)
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			logger.Fatalf(errHttpListen, err)
		}
		logger.Infoln(logHttpStop)
	}()

	// Graceful shutdown setup
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	logger.Infoln(logSignalInterrupt)
	ctxShutdown, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	// Graceful shutdown HTTP Server
	if err := server.Shutdown(ctxShutdown); err != nil {
		logger.Fatalf(errHttpShutdown, err)
	}
	logger.Infoln(logHttpShutdown)

	// Graceful disconnect db client
	if err := vacancyRepository.Disconnect(ctxShutdown); err != nil {
		logger.Fatalf(errDbDisconnect, cfg.Database.Provider, err)
	}
	logger.Infof(logDbDisconnect, cfg.Database.Provider)
}
