// Package classification vacancies-parser.
//
// Documentation of your project API.
//
//	Schemes:
//	- http
//	- https
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Security:
//	- basic
//
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package docs

//go:generate swagger generate spec -o ./public/swagger.json --scan-models
