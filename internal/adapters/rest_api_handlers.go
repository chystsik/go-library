package adapters

import (
	"encoding/json"
	"net/http"
	"os"

	handlers "gitlab.com/chystsik/go-library/internal/adapters/rest_api_handlers"
	"gitlab.com/chystsik/go-library/internal/adapters/swagger"
	"gitlab.com/chystsik/go-library/internal/service"

	"github.com/sirupsen/logrus"
)

type serviceHandlers struct {
	handlers.VacancyHandlers
	swagger.SwaggerUIHandler
	logger logrus.FieldLogger
}

type ServiceHandlers interface {
	Greeting(w http.ResponseWriter, r *http.Request)
	handlers.VacancyHandlers
	swagger.SwaggerUIHandler
}

func NewHandlers(vacancyService service.VacancyService, swaggerTemplate string, logger logrus.FieldLogger) ServiceHandlers {
	return &serviceHandlers{
		handlers.NewVacancyHandlers(vacancyService, logger),
		swagger.NewSwaggerUIHandler(swaggerTemplate, logger),
		logger,
	}
}

func (sh *serviceHandlers) Greeting(w http.ResponseWriter, r *http.Request) {
	hostname, err := os.Hostname()
	if err != nil {
		sh.logger.Errorln(err)
	}

	var greeting = struct {
		Status  string `json:"status"`
		Message string `json:"message"`
		Version string `json:"version"`
		Host    string `json:"hostname"`
	}{
		Status:  "Active",
		Message: "Web server up and running",
		Version: "1.0.0",
		Host:    hostname,
	}

	out, err := json.Marshal(greeting)
	if err != nil {
		sh.logger.Errorln(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(out)
	if err != nil {
		sh.logger.Errorln(err)
	}
}
