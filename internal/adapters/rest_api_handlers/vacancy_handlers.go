package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/chystsik/go-library/internal/adapters/swagger"
	"gitlab.com/chystsik/go-library/internal/service"

	"github.com/sirupsen/logrus"
)

type VacancyHandlers interface {
	SearchVacancies(w http.ResponseWriter, r *http.Request)
	DeleteVacancyByID(w http.ResponseWriter, r *http.Request)
	GetVacancyByID(w http.ResponseWriter, r *http.Request)
	ListVacancies(w http.ResponseWriter, r *http.Request)
}

type vacancyHandlers struct {
	vacancyService service.VacancyService
	logger         logrus.FieldLogger
}

func NewVacancyHandlers(vs service.VacancyService, logger logrus.FieldLogger) VacancyHandlers {
	return &vacancyHandlers{
		vacancyService: vs,
		logger:         logger,
	}
}

// swagger:route POST /search vacancy SearchVacanciesRequest
// Поиск вакансий по ключевому слову
//
// Добавляет найденные вакансии в репозиторий
// responses:
//
//	200: body:JSONResponse Успешная операция
//	400: description: Invalid input
func (vh *vacancyHandlers) SearchVacancies(w http.ResponseWriter, r *http.Request) {
	var (
		query swagger.SearchQuery
		resp  JSONResponse
		err   error
	)

	err = json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		errorJSON(w, err, vh.logger, http.StatusBadRequest)
		return
	}

	err = vh.vacancyService.CreateVacancies(query.Query)
	if err != nil {
		errorJSON(w, err, vh.logger, http.StatusInternalServerError)
		return
	}

	resp.Message = "Created"
	writeJSON(w, http.StatusOK, "application/json", resp, vh.logger)
}

// swagger:route POST /delete vacancy DeleteVacancyRequest
// Удаление вакансии по id
//
// Удаляет вакансию из репозитория
// responses:
//
//	200: body:JSONResponse Успешная операция
//	400: description: Invalid input
func (vh *vacancyHandlers) DeleteVacancyByID(w http.ResponseWriter, r *http.Request) {
	var (
		query swagger.DeleteQuery
		resp  JSONResponse
		err   error
	)

	err = json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		errorJSON(w, err, vh.logger, http.StatusBadRequest)
		return
	}

	err = vh.vacancyService.DeleteVacancy(query.Id)
	if err != nil {
		if err.Error() == fmt.Sprintf("vacancy with id %d not found", query.Id) {
			errorJSON(w, err, vh.logger, http.StatusBadRequest)
			return
		}
		errorJSON(w, err, vh.logger, http.StatusInternalServerError)
		return
	}

	resp.Message = "Deleted"
	writeJSON(w, http.StatusOK, "application/json", resp, vh.logger)
}

// swagger:route POST /get vacancy GetVacancyRequest
// Поиск вакансии по id
//
// Возвращает вакансию
// responses:
//
//	200: body:JSONResponse Успешная операция
//	400: description: Invalid input
func (vh *vacancyHandlers) GetVacancyByID(w http.ResponseWriter, r *http.Request) {
	var (
		query swagger.GetQuery
		resp  JSONResponse
		err   error
	)

	err = json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		errorJSON(w, err, vh.logger, http.StatusBadRequest)
		return
	}

	vacancy, err := vh.vacancyService.GetVacancy(query.Id)
	if err != nil {
		if err.Error() == fmt.Sprintf("vacancy with id %d not found", query.Id) {
			errorJSON(w, err, vh.logger, http.StatusBadRequest)
			return
		}
		errorJSON(w, err, vh.logger, http.StatusInternalServerError)
		return
	}

	resp.Data = vacancy
	resp.Message = "Vacancy"
	writeJSON(w, http.StatusOK, "application/json", resp, vh.logger)
}

// swagger:route GET /list vacancy none
// Список всех вакансий
//
// Возвращает список вакансий
// responses:
//
//	200: body:JSONResponse Успешная операция
func (vh *vacancyHandlers) ListVacancies(w http.ResponseWriter, r *http.Request) {
	var (
		resp JSONResponse
		err  error
	)

	vacancies, err := vh.vacancyService.GetVacancies()
	if err != nil {
		errorJSON(w, err, vh.logger, http.StatusInternalServerError)
		return
	}

	resp.Data = vacancies
	resp.Message = fmt.Sprintf("%d vacancies in repo", len(vacancies))
	writeJSON(w, http.StatusOK, "application/json", resp, vh.logger)
}
