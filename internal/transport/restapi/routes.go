package restapi

import (
	"net/http"

	"gitlab.com/chystsik/go-library/internal/adapters"

	"github.com/go-chi/chi/v5"
)

func registerHandlers(router *chi.Mux, handlers adapters.ServiceHandlers, publicFolder string) {
	// Greeting
	router.Get("/", handlers.Greeting)

	// SwaggerUI
	router.Get("/swagger", handlers.SwaggerUI)
	router.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir(publicFolder))).ServeHTTP(w, r)
	})

	// Parser
	router.Post("/search", handlers.SearchVacancies)
	router.Post("/delete", handlers.DeleteVacancyByID)
	router.Post("/get", handlers.GetVacancyByID)
	router.Get("/list", handlers.ListVacancies)
}
