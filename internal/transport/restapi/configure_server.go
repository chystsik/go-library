package restapi

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/chystsik/go-library/config"
	"gitlab.com/chystsik/go-library/internal/adapters"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
)

func NewServer(cfg config.AppConfig, handlers adapters.ServiceHandlers, logger *logrus.Logger) *http.Server {
	// Set up a basic chi router
	router := chi.NewRouter()

	router.Use(middleware.RequestLogger(&StructuredLogger{Logger: logger}))
	router.Use(middleware.Recoverer)

	// Register handlers
	registerHandlers(router, handlers, cfg.Service.PublicFolder)

	Server := &http.Server{
		Addr:           fmt.Sprintf("%s:%d", cfg.Http.Host, cfg.Http.Port),
		Handler:        router,
		ReadTimeout:    40 * time.Second,
		WriteTimeout:   40 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return Server
}

type StructuredLogger struct {
	Logger *logrus.Logger
}

type LogEntry struct {
	entry *logrus.Entry
}

func (l *StructuredLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	uri := fmt.Sprintf("%s://%s%s - %s:%s", scheme, r.Host, r.RequestURI, r.Proto, r.Method)
	logFields := logrus.Fields{
		"method":      r.Method,
		"uri":         uri,
		"remote_addr": r.RemoteAddr,
	}
	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["request_id"] = reqID
	}

	entry := logrus.NewEntry(l.Logger).WithFields(logFields)
	//entry.Debug("request started")

	return &LogEntry{entry: entry}
}

func (l *LogEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	entry := l.entry.WithFields(logrus.Fields{
		"resp_status":     status,
		"resp_bytes":      bytes,
		"resp_latency_ms": float64(elapsed.Nanoseconds()) / 1000000.0,
	})

	// Debug log if status code is 200/201/eg
	if status > 500 {
		entry.Warn("request")
		return
	}
	entry.Debug("request")
}

func (l *LogEntry) Panic(v interface{}, stack []byte) {
	l.entry.WithFields(logrus.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	}).Errorln("request panicked")
}
