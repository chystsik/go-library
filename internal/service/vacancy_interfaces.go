package service

import (
	"context"

	"gitlab.com/chystsik/go-library/internal/models"
)

type VacancyRepository interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
	Connect(ctx context.Context) error
	Disconnect(ctx context.Context) error
}

type VacancyParser interface {
	GetLinks(query string) ([]string, error)
	ParseLinks(links []string) ([]models.Vacancy, error)
}

type VacancyService interface {
	CreateVacancies(query string) error
	GetVacancy(id int) (models.Vacancy, error)
	GetVacancies() ([]models.Vacancy, error)
	DeleteVacancy(id int) error
}
