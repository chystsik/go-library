package service

import (
	"gitlab.com/chystsik/go-library/internal/models"
)

type vacancyService struct {
	vacancyRepo   VacancyRepository
	vacancyParser VacancyParser
}

func NewVacancyService(vr VacancyRepository, vp VacancyParser) VacancyService {
	return &vacancyService{
		vacancyRepo:   vr,
		vacancyParser: vp,
	}
}

func (vs *vacancyService) CreateVacancies(query string) error {
	links, err := vs.vacancyParser.GetLinks(query)
	if err != nil {
		return err
	}

	vacancies, err := vs.vacancyParser.ParseLinks(links)
	if err != nil {
		return err
	}

	for i := range vacancies {
		err := vs.vacancyRepo.Create(vacancies[i])
		if err != nil {
			return err
		}
	}

	return nil
}

func (vs *vacancyService) GetVacancy(id int) (models.Vacancy, error) {
	return vs.vacancyRepo.GetByID(id)
}

func (vs *vacancyService) GetVacancies() ([]models.Vacancy, error) {
	return vs.vacancyRepo.GetList()
}

func (vs *vacancyService) DeleteVacancy(id int) error {
	return vs.vacancyRepo.Delete(id)
}
