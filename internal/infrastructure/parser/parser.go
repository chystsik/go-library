package parser

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/chystsik/go-library/config"
	"gitlab.com/chystsik/go-library/internal/models"
	"gitlab.com/chystsik/go-library/internal/service"

	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"golang.org/x/sync/errgroup"
)

type vacancyParser struct {
	urlPrefix string
	caps      selenium.Capabilities
	chrCaps   chrome.Capabilities
	conf      config.Parser
	doc       *goquery.Document
	logger    logrus.FieldLogger
}

func NewVacancyParser(conf config.Parser, logger logrus.FieldLogger) service.VacancyParser {
	var parser vacancyParser

	// прописываем конфигурацию для драйвера
	parser.caps = selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	parser.chrCaps = chrome.Capabilities{
		W3C: true,
	}
	parser.caps.AddChrome(parser.chrCaps)

	// прописываем адрес нашего драйвера
	parser.urlPrefix = fmt.Sprintf("http://%s:%d/wd/hub", conf.SeleniumHost, conf.SeleniumPort)

	// прописываем параметры парсинга
	parser.conf = conf

	parser.logger = logger

	return &parser
}

// GetLinks returns vacansies links
func (vp *vacancyParser) GetLinks(query string) ([]string, error) {
	var (
		links      []string
		pagesCount uint
		err        error
	)

	// получаем количество страниц поиска с вакансиями
	pagesCount, err = vp.getPagesCount(query)
	if err != nil {
		return nil, err
	}

	// получаем ссылки на вакансии с каждой страницы поиска
	links, err = vp.getLinks(pagesCount, query)
	if err != nil {
		return nil, err
	}

	return links, nil
}

// ParseLinks parses vacancy pages by links
func (vp *vacancyParser) ParseLinks(links []string) ([]models.Vacancy, error) {
	vacancies := make([]models.Vacancy, 0, len(links))
	var unmErrCnt, unmWarnCount uint

	var mu sync.Mutex
	g := errgroup.Group{}
	g.SetLimit(25)

	for i := range links {

		i := i
		g.Go(func() error {
			resp, err := http.Get(links[i])
			if err != nil {
				return err
			}

			vp.doc, err = goquery.NewDocumentFromReader(resp.Body)
			if err != nil && vp.doc != nil {
				return err
			}

			dd := vp.doc.Find("script[type=\"application/ld+json\"]")
			if dd == nil {
				return errors.New("vacancy nodes not found")
			}

			ss := dd.First().Text()

			var vacancy models.Vacancy

			err = json.Unmarshal([]byte(ss), &vacancy)
			if err != nil {
				switch err.(type) {
				case *json.SyntaxError: // bad json
					vp.logger.Errorf("vacancy %s unmarshaling error: %v", links[i], err)
					unmErrCnt++
					return nil
				case *json.UnmarshalTypeError: // has no address
					//vp.logger.Warningf("vacancy %s fields unmarshaling error: %v", links[i], err)
					unmWarnCount++
				}
			}

			idRaw := strings.TrimPrefix(links[i], fmt.Sprintf("%s/vacancies/", vp.conf.VacancySiteUrl))
			vacancy.Id, err = strconv.Atoi(idRaw)
			if err != nil {
				return err
			}

			mu.Lock()

			vacancies = append(vacancies, vacancy)

			mu.Unlock()

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	vp.logger.Infof("vacancies parsed %d: with warnings %d, not parsed %d with errors", len(vacancies), unmWarnCount, unmErrCnt)

	return vacancies, nil
}

// getPagesCount gets pages count for the total vacancies found
func (vp *vacancyParser) getPagesCount(query string) (uint, error) {
	var (
		wd         selenium.WebDriver
		err        error
		pagesCount uint
	)

	// немного костылей чтобы драйвер не падал
	i := 1
	for i < 5 {
		wd, err = selenium.NewRemote(vp.caps, vp.urlPrefix)
		if err != nil {
			vp.logger.Warning(err)
			i++
			continue
		}
		break
	}

	// обращаемся к странице поиска вакансий
	err = wd.Get(fmt.Sprintf(vp.conf.VacancySearchUrl, 1, query))
	if err != nil {
		return 0, err
	}

	// количество найденных вакансий
	elem, err := wd.FindElement(selenium.ByCSSSelector, vp.conf.Selectors.VacancyCount)
	if err != nil {
		return 0, err
	}

	vacancyCountStr, err := elem.Text()
	if err != nil {
		return 0, err
	}

	// закрываем сессию
	err = wd.Quit()
	if err != nil {
		return 0, err
	}

	var vacancyCountRaw strings.Builder
	for i := 0; i < len(vacancyCountStr); i++ {
		b := vacancyCountStr[i]
		if '0' <= b && b <= '9' {
			vacancyCountRaw.WriteByte(b)
		}
	}

	vacancyCount, err := strconv.Atoi(vacancyCountRaw.String())
	if err != nil {
		return 0, err
	}

	// рассчитываем количество страниц
	pagesCount = uint(vacancyCount) / vp.conf.VacanciesPerPage
	if uint(vacancyCount)%vp.conf.VacanciesPerPage != 0 {
		pagesCount += 1
	}

	vp.logger.Infof("found %d vacancies for %s query on %d pages", vacancyCount, query, pagesCount)

	return pagesCount, nil
}

// getLinks returns vacancies links
func (vp *vacancyParser) getLinks(pagesCount uint, query string) ([]string, error) {
	var (
		links []string
		page  uint
	)

	var mu sync.Mutex
	g := errgroup.Group{}
	g.SetLimit(int(pagesCount))

	for page = 1; page <= pagesCount; page++ {

		page := page
		g.Go(func() error {
			wd, err := selenium.NewRemote(vp.caps, vp.urlPrefix)
			if err != nil {
				return err
			}

			err = wd.Get(fmt.Sprintf(vp.conf.VacancySearchUrl, page, query))
			if err != nil {
				return err
			}

			elems, err := wd.FindElements(selenium.ByCSSSelector, vp.conf.Selectors.OnPageLinks)
			if err != nil {
				return err
			}

			for i := range elems {
				var link string

				link, err = elems[i].GetAttribute("href")
				if err != nil {
					return err
				}
				mu.Lock()
				links = append(links, fmt.Sprintf("%s%s", vp.conf.VacancySiteUrl, link))
				mu.Unlock()
			}
			return wd.Quit()
		})
	}
	if err := g.Wait(); err != nil {
		vp.logger.Error(err)
		return nil, err
	}

	vp.logger.Infof("saved %d vacancies links from %d pages", len(links), pagesCount)

	return links, nil
}
