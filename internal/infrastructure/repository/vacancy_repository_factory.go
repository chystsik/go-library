package repository

import (
	"fmt"

	"gitlab.com/chystsik/go-library/config"
	"gitlab.com/chystsik/go-library/internal/infrastructure/repository/mongodb"
	"gitlab.com/chystsik/go-library/internal/infrastructure/repository/postgresql"
	"gitlab.com/chystsik/go-library/internal/service"

	"github.com/sirupsen/logrus"
)

const (
	errWrognProvider = "DB provider %s is not allowed"
)

func NewVacancyRepository(cfg config.Database, logger logrus.FieldLogger) (service.VacancyRepository, error) {
	switch cfg.Provider {
	case "mongodb":
		return mongodb.NewVacancyRepository(cfg.Mongodb, logger), nil
	case "postgresql":
		return postgresql.NewVacancyRepository(cfg.Postgresql, logger), nil
	default:
		return nil, fmt.Errorf(errWrognProvider, cfg.Provider)
	}
}
