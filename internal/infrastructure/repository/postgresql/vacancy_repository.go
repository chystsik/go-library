package postgresql

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/chystsik/go-library/config"
	"gitlab.com/chystsik/go-library/internal/models"
	"gitlab.com/chystsik/go-library/internal/service"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v5/pgconn"
	_ "github.com/jackc/pgx/v5/stdlib" // Standard library bindings for pgx
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

const (
	errVacancyNotFound   = "vacancy with id %d not found"
	errVacancyDuplicated = "vacancy with id %d already exist"
	errVacancyCreate     = "cant create new vacancy: %v"
	errInitDbSchema      = "cant init db schema: %v"

	errClientConnectTimeout = "postgres client connection timeout: %v. With URI: %s"
	errClientPingTimeout    = "postgres client ping timeout: %v. With URI: %s"
	errCreateCollection     = "cant create collectioon %s: %v"
)

type vacancyRepository struct {
	cfg    *config.Postgresql
	repo   *sqlx.DB
	uri    string
	logger logrus.FieldLogger
}

func NewVacancyRepository(cfg config.Postgresql, logger logrus.FieldLogger) service.VacancyRepository {
	connPattern := "postgres://%v:%v@%v:%v/%v?sslmode=disable"
	if cfg.User == "" {
		connPattern = "postgres://%s%s%v:%v/%v?sslmode=disable"
	}

	uri := fmt.Sprintf(connPattern,
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.DbName,
	)

	schema := fmt.Sprintf("file://%s", cfg.Schema)
	cfg.Schema = schema

	return &vacancyRepository{
		cfg:    &cfg,
		uri:    uri,
		logger: logger,
	}
}

func (vr *vacancyRepository) Create(dto models.Vacancy) error {
	ds := fromDomainVacancy(dto)

	query := fmt.Sprintf("insert into vacancy (%s) values (%s) on conflict (id) do update set %s", dsVacancyQueryFields, dsVacancyQueryValues, dsVacancyQueryUpsert)
	_, err := vr.repo.NamedExec(query, ds)
	if err != nil {
		return err
	}

	return nil
}

func (vr *vacancyRepository) GetByID(id int) (models.Vacancy, error) {
	var ds dsVacancy

	query := fmt.Sprintf("select * from vacancy where id=%d", id)

	err := vr.repo.Get(&ds, query)
	if err != nil {
		return models.Vacancy{}, fmt.Errorf(errVacancyNotFound, id)
	}

	return toDomainVacancy(ds), nil
}

func (vr *vacancyRepository) GetList() ([]models.Vacancy, error) {
	ds := []dsVacancy{}

	err := vr.repo.Select(&ds, "select * from vacancy order by company_name asc")
	if err != nil {
		return nil, err
	}

	return toDomainVacancies(ds), nil
}

func (vr *vacancyRepository) Delete(id int) error {
	res, err := vr.repo.Exec("delete from vacancy where id=$1", id)
	if err != nil {
		return err
	}

	count, err := res.RowsAffected()
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Errorf(errVacancyNotFound, id)
		}
		return err
	}

	if count == 0 {
		return fmt.Errorf(errVacancyNotFound, id)
	}

	return nil
}

func (vr *vacancyRepository) Connect(ctx context.Context) error {
	var err error
	var m *migrate.Migrate

	// create db if not exist
	vr.repo, err = sqlx.Connect("pgx", fmt.Sprintf("host=%s port=%d user=%s password=%s sslmode=%s",
		vr.cfg.Host, vr.cfg.Port, vr.cfg.User, vr.cfg.Password, "disable"))
	if err != nil {
		return err
	}

	_, err = vr.repo.Exec(fmt.Sprintf("CREATE DATABASE %s", vr.cfg.DbName))
	if err != nil {
		pgEr, ok := err.(*pgconn.PgError)
		if !ok { // not pgconn error
			return err
		} else if pgEr.Code != "42P04" { // database <db_name> already exists (SQLSTATE 42P04)
			return err
		}
		vr.logger.Infoln(err.(*pgconn.PgError).Message)
	} else {
		vr.logger.Infof("database with name \"%s\" was created", vr.cfg.DbName)
	}

	// perform migration
	m, err = migrate.New(vr.cfg.Schema, vr.uri)
	if err != nil {
		return fmt.Errorf(errInitDbSchema, err)
	}

	err = m.Up()
	if err != nil {
		vr.logger.Infof("performing schema update: %v", err)
	} else {
		vr.logger.Infoln("database schema was created")
	}

	// open db
	vr.repo, err = sqlx.Open("pgx", vr.uri)
	if err != nil {
		return err
	}

	timeout := time.Duration(10) * time.Second
	ctxTimeout, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	if err := vr.repo.PingContext(ctxTimeout); err != nil {
		return fmt.Errorf(errClientPingTimeout, timeout, vr.uri)
	}

	return nil
}

func (vr *vacancyRepository) Disconnect(ctx context.Context) error {
	if err := vr.repo.Close(); err != nil {
		return err
	}
	return nil
}
