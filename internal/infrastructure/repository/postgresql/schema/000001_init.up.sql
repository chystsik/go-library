create table vacancy (
	id serial primary key not null unique,
	posted_at varchar(10),
	expire_at varchar(10),
    employment_type varchar(60),
    location_address varchar(200),
    location_type varchar(33),
    company_name varchar(50),
    company_site varchar(260),
    salary_currency varchar(10),
    salary_min bigint default 0,
    salary_max bigint default 0,
    title varchar(300),
    description varchar(12000)
);