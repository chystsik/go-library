package postgresql

import (
	"gitlab.com/chystsik/go-library/internal/models"

	"strings"
)

const (
	dsVacancyQueryFields = `id, posted_at, expire_at, employment_type, location_type, location_address, salary_currency, salary_min, salary_max, title, description, company_name, company_site`
	dsVacancyQueryUpsert = `posted_at=:posted_at, expire_at=:expire_at, employment_type=:employment_type, location_type=:location_type, location_address=:location_address, salary_currency=:salary_currency, salary_min=:salary_min, salary_max=:salary_max, title=:title, description=:description, company_name=:company_name, company_site=:company_site`
	dsVacancyQueryValues = `:id, :posted_at, :expire_at, :employment_type, :location_type, :location_address, :salary_currency, :salary_min, :salary_max, :title, :description, :company_name, :company_site`
)

type dsVacancy struct {
	Id             int    `db:"id"`
	Posted         string `db:"posted_at"`
	Expire         string `db:"expire_at"`
	EmploymentType string `db:"employment_type"`
	Location       string `db:"location_address"` //Location []DsJobLocation `db:"location_address"`
	LocationType   string `db:"location_type"`
	DsCompany
	DsSalary
	DsInfo
}

/*
	 type DsJobLocation struct {
		Address string
	}
*/
type DsSalary struct {
	Currency string `db:"salary_currency"`
	DsSalaryValue
}

type DsSalaryValue struct {
	Min float64 `db:"salary_min"`
	Max float64 `db:"salary_max"`
}

type DsInfo struct {
	Title       string `db:"title"`
	Description string `db:"description"`
}

type DsCompany struct {
	Name    string `db:"company_name"`
	WebSite string `db:"company_site"`
}

func toDomainJobLocations(ds string) []models.JobLocation {
	locations := strings.Split(ds, ",")
	result := make([]models.JobLocation, len(locations))

	for i := range locations {
		result[i].Address = locations[i]
	}

	return result
}

func fromDomainJobLocations(d []models.JobLocation) string {
	var locations strings.Builder

	for i := range d {
		locations.WriteString(d[i].Address)
		if i < len(d)-1 {
			locations.WriteString(",")
		}
	}

	return locations.String()
}

func toDomainVacancy(ds dsVacancy) models.Vacancy {
	return models.Vacancy{
		Id:             ds.Id,
		Posted:         ds.Posted,
		Expire:         ds.Expire,
		EmploymentType: ds.EmploymentType,
		Location:       toDomainJobLocations(ds.Location),
		LocationType:   ds.LocationType,
		Company: models.Company{
			Name:    ds.DsCompany.Name,
			WebSite: ds.DsCompany.WebSite,
		},
		Salary: models.Salary{
			Currency: ds.DsSalary.Currency,
			SalaryValue: models.SalaryValue{
				Min: ds.DsSalary.DsSalaryValue.Min,
				Max: ds.DsSalary.DsSalaryValue.Max,
			},
		},
		Info: models.Info{
			Title:       ds.DsInfo.Title,
			Description: ds.DsInfo.Description,
		},
	}
}

func fromDomainVacancy(d models.Vacancy) dsVacancy {
	return dsVacancy{
		Id:             d.Id,
		Posted:         d.Posted,
		Expire:         d.Expire,
		EmploymentType: d.EmploymentType,
		Location:       fromDomainJobLocations(d.Location),
		LocationType:   d.LocationType,
		DsCompany: DsCompany{
			Name:    d.Company.Name,
			WebSite: d.Company.WebSite,
		},
		DsSalary: DsSalary{
			Currency: d.Salary.Currency,
			DsSalaryValue: DsSalaryValue{
				Min: d.Salary.SalaryValue.Min,
				Max: d.Salary.SalaryValue.Max,
			},
		},
		DsInfo: DsInfo{
			Title:       d.Info.Title,
			Description: d.Info.Description,
		},
	}
}

func toDomainVacancies(ds []dsVacancy) []models.Vacancy {
	result := make([]models.Vacancy, len(ds))

	for i := range ds {
		result[i] = toDomainVacancy(ds[i])
	}

	return result
}
