package mongodb

import "gitlab.com/chystsik/go-library/internal/models"

type dsVacancy struct {
	Id             int             `bson:"vacancy_id"`
	Posted         string          `bson:"posted_at"`
	Expire         string          `bson:"expire_at"`
	EmploymentType string          `bson:"employment_type"`
	Location       []dsJobLocation ` bson:"location"`
	LocationType   string          `bson:"location_type"`
	Company        dsCompany       `bson:"company"`
	Salary         dsSalary        `bson:"salary"`
	Info           dsInfo
}

type dsJobLocation struct {
	Address string `bson:"address"`
}

type dsSalary struct {
	Currency    string        `bson:"currency"`
	SalaryValue dsSalaryValue `bson:"value"`
}

type dsSalaryValue struct {
	Min float64 `bson:"min"`
	Max float64 `bson:"max"`
}

type dsInfo struct {
	Title       string `bson:"title"`
	Description string `bson:"description"`
}

type dsCompany struct {
	Name    string `bson:"name"`
	WebSite string `bson:"web_site"`
}

func toDomainJobLocations(ds []dsJobLocation) []models.JobLocation {
	result := make([]models.JobLocation, len(ds))

	for i := range ds {
		result[i] = models.JobLocation{Address: ds[i].Address}
	}

	return result
}

func fromDomainJobLocations(d []models.JobLocation) []dsJobLocation {
	result := make([]dsJobLocation, len(d))

	for i := range d {
		result[i] = dsJobLocation{Address: d[i].Address}
	}

	return result
}

func toDomainVacancy(ds dsVacancy) models.Vacancy {
	return models.Vacancy{
		Id:             ds.Id,
		Posted:         ds.Posted,
		Expire:         ds.Expire,
		EmploymentType: ds.EmploymentType,
		Location:       toDomainJobLocations(ds.Location),
		LocationType:   ds.LocationType,
		Company: models.Company{
			Name:    ds.Company.Name,
			WebSite: ds.Company.WebSite,
		},
		Salary: models.Salary{
			Currency: ds.Salary.Currency,
			SalaryValue: models.SalaryValue{
				Min: ds.Salary.SalaryValue.Min,
				Max: ds.Salary.SalaryValue.Max,
			},
		},
		Info: models.Info{
			Title:       ds.Info.Title,
			Description: ds.Info.Description,
		},
	}
}

func fromDomainVacancy(d models.Vacancy) dsVacancy {
	return dsVacancy{
		Id:             d.Id,
		Posted:         d.Posted,
		Expire:         d.Expire,
		EmploymentType: d.EmploymentType,
		Location:       fromDomainJobLocations(d.Location),
		LocationType:   d.LocationType,
		Company: dsCompany{
			Name:    d.Company.Name,
			WebSite: d.Company.WebSite,
		},
		Salary: dsSalary{
			Currency: d.Salary.Currency,
			SalaryValue: dsSalaryValue{
				Min: d.Salary.SalaryValue.Min,
				Max: d.Salary.SalaryValue.Max,
			},
		},
		Info: dsInfo{
			Title:       d.Info.Title,
			Description: d.Info.Description,
		},
	}
}

func toDomainVacancies(ds []dsVacancy) []models.Vacancy {
	result := make([]models.Vacancy, len(ds))

	for i := range ds {
		result[i] = toDomainVacancy(ds[i])
	}

	return result
}
