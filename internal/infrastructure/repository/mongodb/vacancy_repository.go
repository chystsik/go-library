package mongodb

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/chystsik/go-library/config"
	"gitlab.com/chystsik/go-library/internal/models"
	"gitlab.com/chystsik/go-library/internal/service"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const (
	errVacancyNotFound   = "vacancy with id %d not found"
	errVacancyDuplicated = "vacancy with id %d already exist"
	errVacancyCreate     = "cant create new vacancy: %v"

	errClientConnectTimeout = "mongo client connection timeout: %v. With URI: %s"
	errClientPingTimeout    = "mongo client ping timeout: %v. With URI: %s"
	errCreateCollection     = "cant create collectioon %s: %v"
)

type vacancyRepository struct {
	repo   *mongo.Collection
	opts   *options.ClientOptions
	client *mongo.Client
	logger logrus.FieldLogger
}

func NewVacancyRepository(cfg config.Mongodb, logger logrus.FieldLogger) service.VacancyRepository {
	connPattern := "mongodb://%v:%v@%v:%v"
	if cfg.User == "" {
		connPattern = "mongodb://%s%s%v:%v"
	}

	uri := fmt.Sprintf(connPattern,
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Port,
	)

	return &vacancyRepository{
		opts:   options.Client().ApplyURI(uri),
		logger: logger}
}

func (vr *vacancyRepository) Create(dto models.Vacancy) error {
	ds := fromDomainVacancy(dto)

	_, err := vr.repo.InsertOne(context.TODO(), ds)
	if mongo.IsDuplicateKeyError(err) {

		// update vacancy if it exist
		_, err = vr.repo.UpdateByID(context.TODO(), bson.M{"vacancy_id": ds.Id}, bson.M{"$set": ds})
		if err != nil {
			return err
		}
		return nil
	}
	if err != nil {
		return fmt.Errorf(errVacancyCreate, err)
	}

	return nil
}

func (vr *vacancyRepository) GetByID(id int) (models.Vacancy, error) {
	var result dsVacancy
	err := vr.repo.FindOne(context.TODO(), bson.M{"vacancy_id": id}).Decode(&result)
	if err == mongo.ErrNoDocuments {
		return models.Vacancy{}, fmt.Errorf(errVacancyNotFound, id)
	}
	if err != nil {
		return models.Vacancy{}, err
	}

	return toDomainVacancy(result), nil
}

func (vr *vacancyRepository) GetList() ([]models.Vacancy, error) {
	var result []dsVacancy

	// pass these options to the Find method
	findOptions := options.Find()

	// Set the limit of the number of record to find
	findOptions.SetLimit(1000)

	// Passing the bson.D{{}} as the filter matches  documents in the collection
	cursor, err := vr.repo.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		return nil, err
	}

	// Finding multiple documents returns a cursor
	// Iterate through the cursor allows us to decode documents one at a time using All func
	err = cursor.All(context.TODO(), &result)
	if err != nil {
		return nil, err
	}

	//Close the cursor once finished
	cursor.Close(context.TODO())

	return toDomainVacancies(result), nil
}

func (vr *vacancyRepository) Delete(id int) error {
	result, err := vr.repo.DeleteOne(context.TODO(), bson.M{"vacancy_id": id})
	if err != nil {
		return err
	}
	if result.DeletedCount != 1 {
		return fmt.Errorf(errVacancyNotFound, id)
	}

	return nil
}

func (vr *vacancyRepository) Connect(ctx context.Context) error {
	var err error
	var dbName string = "go-parser"
	var collName string = "vacancy"

	vr.client, err = mongo.NewClient(vr.opts)
	if err != nil {
		return err
	}

	timeout := time.Duration(10) * time.Second
	uri := vr.opts.GetURI()
	ctxTimeout, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	if err := vr.client.Connect(ctxTimeout); err != nil {
		return fmt.Errorf(errClientConnectTimeout, timeout, uri)
	}
	if err := vr.client.Ping(ctxTimeout, readpref.Primary()); err != nil {
		return fmt.Errorf(errClientPingTimeout, err, uri)
	}

	db := vr.client.Database(dbName)
	vr.repo = db.Collection(collName)

	// create unique index
	_, err = vr.repo.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: "vacancy_id", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	)
	if err != nil {
		return fmt.Errorf(errCreateCollection, collName, err)
	}

	return nil
}

func (vr *vacancyRepository) Disconnect(ctx context.Context) error {
	if err := vr.client.Disconnect(ctx); err != nil {
		return err
	}
	return nil
}
